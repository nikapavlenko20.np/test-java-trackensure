import matrix.Matrix;
import org.junit.jupiter.api.Test;
import palindrome.Palindrome;

import static org.junit.jupiter.api.Assertions.*;

public class Task2 {
    private final Matrix matrix = new Matrix();

    @Test
    void test1() {
        int[][] m = {{9,8,7},{6,5,4},{3,2,1}};
        int[][] expect = {{3,6,9},{2,5,8},{1,4,7}};
        int[][] solve = matrix.solve(m);

        assertEquals(solve, expect);
    }
}
