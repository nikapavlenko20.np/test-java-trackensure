import matrix.Matrix;
import org.junit.jupiter.api.Test;
import storageTank.StorageTank;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task3 {

    private final StorageTank tank = new StorageTank();

    @Test
    void test1() {
        int[] k = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
        int expect = 6;
        int solve = tank.solve(k);

        assertEquals(solve, 6);
    }
}
