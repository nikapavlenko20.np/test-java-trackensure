import org.junit.jupiter.api.Test;
import palindrome.Palindrome;

import static org.junit.jupiter.api.Assertions.*;

public class Task1 {
    private final Palindrome palindrome = new Palindrome();

    @Test
    void test1() {
        int k = 323;
        assertTrue(palindrome.solve(k));
    }

    @Test
    void test2() {
        int k = -45;
        assertFalse(palindrome.solve(k));
    }
}
