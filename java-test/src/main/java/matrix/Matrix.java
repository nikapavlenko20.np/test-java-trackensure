package matrix;

public class Matrix {

    public int[][] solve(int[][] m) {
        int n = m.length;

        for(int i = 0; i < n; ++i) {
            for(int j = i + 1; j < n; ++j) {
                int num = m[i][j];
                m[i][j] = m[j][i];
                m[j][i] = num;
            }
        }

        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n/2; ++j) {
                int num = m[i][n - j - 1];
                m[i][n - j - 1] = m[i][j];
                m[i][j] = num;
            }
        }

        return m;
    }
}
