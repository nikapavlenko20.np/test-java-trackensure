package storageTank;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StorageTank {


    public static int solve(int[] k) {
        int l = k.length;
        if (l < 2) return 0;

        int left = 0;
        int rigth = 0;
        int leftMax = 0;
        int rigthMax = 0;
        int maxrea = 0;
        Stack<Integer> leftMaxArr = new Stack<Integer>();
        Stack<Integer> rightMaxArr = new Stack<>();

        for (int i = 0; i< l; i++) {
            left = i;
            rigth = l - 1- i;

            if(k[left] > leftMax) leftMax = k[left];
            leftMaxArr.add(leftMax);
            if(k[rigth] > rigthMax) rigthMax = k[rigth];
            rightMaxArr.push(rigthMax);
        }

        for (int i = 0; i< l; i++) {
            if (leftMaxArr.peek() <= rightMaxArr.peek())  maxrea += leftMaxArr.get(i) - k[i];
            else maxrea += rightMaxArr.get(i) - k[i];
        }

        return maxrea;
    }
}
