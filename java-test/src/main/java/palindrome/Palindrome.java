package palindrome;

public class Palindrome {
    public boolean solve(int k) {
        int palindrome = 0;
        int newNum = k;
        int saveNum;

        while(newNum != 0){
            saveNum = newNum % 10;
            palindrome = (palindrome * 10) + saveNum;
            newNum = newNum / 10;
        }

       return k == palindrome;
    }
}
