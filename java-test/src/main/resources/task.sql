select CAST((
                  (count(case status when 'cancelled_by_driver' then 1 else null end)
            +  count(case status when 'cancelled_by_client' then 1 else null end))
                         / count(case Status when 'completed' then 1 else null end)) as double precision) Cancellation_rate,

       date
from
              (select status, u.Banned, date from Trips join Users u on trips.client_id = u.id) t
where t.banned = 'NO' group by Request_at;